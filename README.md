## Source Code

You can also clone the from the https://github.com/rhildred/es6test.git

## Softwares required to run the project.

1. Install Git
2. Install Node.js
3. Install Visual Studio Code

## Steps to run the project.

1. You need to clone the complete project into a folder.
2. Open the project folder in the Visual Studio Code.
3. Open the terminal in the visual studio code and run the command "npm install".
4. To get the output click "ctrl + F5".

** These are all the steps to run the project **

** Note :** I have used "GNU General Public License (GPL)" version 3.